package com.example.books.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.books.model.BookRepo
import com.example.books.view.BookState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(private val repo: BookRepo) : ViewModel() {

    private val state = MutableLiveData(BookState(true))
    val stateBook: LiveData<BookState> get() = state

    init {
        viewModelScope.launch {
            val books = repo.getBooks()
            state.value = BookState(books = books)
        }
    }


}