package com.example.books.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.books.Adapter.BookAdapter
import com.example.books.databinding.FragmentBookBinding
import com.example.books.viewModel.BookViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BookFragment : Fragment() {

    private var _binding: FragmentBookBinding? = null
    private val binding get() = _binding!!
    private val bookAdapter by lazy { BookAdapter() }
    private val bookViewModel by viewModels<BookViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentBookBinding.inflate(
        inflater,
        container,
        false
    )
        .also { _binding = it }.root


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvList.adapter = bookAdapter
        bookViewModel.stateBook.observe(viewLifecycleOwner) { state ->

            Log.d("bookfragment", state.books.toString())

            bookAdapter.addBooks(state.books)
        }
    }

}