package com.example.books.view

import com.example.books.model.local.entity.Book

class BookState(
    val isLoading: Boolean = false,
    val books: List<Book> = emptyList()

)