package com.example.books.Adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.books.databinding.ItemBookBinding
import com.example.books.model.local.entity.Book

class BookAdapter : RecyclerView.Adapter<BookAdapter.BookViewHolder>(){

    private var books = mutableListOf<Book>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int
    )= BookViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val book = books[position]
        holder.bindBook(book)
    }

    override fun getItemCount(): Int = books.size

    fun addBooks(book: List<Book>){
        this.books = book.toMutableList()
        notifyDataSetChanged()
    }

    class BookViewHolder(
        private val binding: ItemBookBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindBook(book: Book) = with(
            binding
        ){
            binding.books.text = book.title
        }

        companion object {
            fun getInstance(
                parent: ViewGroup
            ) = ItemBookBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).let { binding -> BookViewHolder(binding) }

        }
    }
}