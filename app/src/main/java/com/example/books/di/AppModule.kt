package com.example.books.di

import android.content.Context
import com.example.books.model.local.BookDatabase
import com.example.books.model.local.dao.BookDao
import com.example.books.model.remote.BookService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun providesBookService() : BookService = BookService.getInstance()

    @Provides
    @Singleton
    fun providesBookDao(@ApplicationContext context: Context) : BookDao = BookDatabase.getInstance(context).bookDao()
}