package com.example.books.model.remote

import com.example.books.model.response.BookDTO
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET

interface BookService {

    companion object{
        private const val BASE_URL = "https://the-dune-api.herokuapp.com"



        fun getInstance() = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<BookService>()
    }

    @GET("/books/100")
    suspend fun getBooks(): List<BookDTO>
}