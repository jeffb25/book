package com.example.books.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.books.model.local.entity.Book
import com.example.books.model.response.BookDTO


@Dao
interface BookDao {
    @Query("SELECT * FROM book_table")
    suspend fun getAll() : List<Book>

    @Insert
    suspend fun insert(bookDTO: List<Book>)
}