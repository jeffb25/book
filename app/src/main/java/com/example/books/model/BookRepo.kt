package com.example.books.model

import com.example.books.model.local.dao.BookDao
import com.example.books.model.local.entity.Book
import com.example.books.model.remote.BookService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BookRepo @Inject constructor(
    private val bookService: BookService,
    private val bookDao: BookDao
) {

    suspend fun getBooks() = withContext(Dispatchers.IO) {
        val cachedbooks: List<Book> = bookDao.getAll()
        return@withContext cachedbooks.ifEmpty {
            val bookData = bookService.getBooks()
            val books: List<Book> = bookData.map {
                Book(title = it.title)
            }
            bookDao.insert(books)
            return@ifEmpty books

        }
    }


}